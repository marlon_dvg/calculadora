package com.calculadora.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.calculadora.demo.model.Servicio;

public interface ServicioRepository extends MongoRepository<Servicio, Integer> {

	List<Servicio> findByIdServicio(Integer idServicio);

}
