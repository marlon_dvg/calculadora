package com.calculadora.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.calculadora.demo.model.Servicio;
import com.calculadora.demo.repository.ServicioRepository;

@SpringBootApplication
@RestController
@RequestMapping("/api")
public class CalculadoraController {

	private static final Logger log = LoggerFactory.getLogger(CalculadoraController.class);

	

	@Autowired
	private ServicioRepository servicioRepository;

	@PostMapping("/servicio")
	public ResponseEntity<Map<String, String>> guardarServicio(@RequestBody Servicio servicio) {

		Servicio servicioModel = servicioRepository.save(servicio);
		log.info("Saved quote=" + servicioModel.toString());

		Map<String,String> res = new HashMap<String,String>();
		res.put("message", "Registro guardado exitosamente");

		return new ResponseEntity<Map<String, String>>(res, HttpStatus.OK);
	}

}
