package com.calculadora.demo.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Servicios")
public class Servicio  {

	private Long id;	
	private Integer idTecnico;	
	private Integer idServicio;		
	private Date fechaInicio;	
	private Date fechaFin;
			
	public Servicio() {
		super();
	}

	public Servicio(Long id, Integer idTecnico, Integer idServicio, Date fechaInicio, Date fechaFin) {
		super();		
		this.idTecnico = idTecnico;
		this.idServicio = idServicio;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getIdTecnico() {
		return idTecnico;
	}

	public void setIdTecnico(Integer idTecnico) {
		this.idTecnico = idTecnico;
	}

	public Integer getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Override
	public String toString() {
		return "Servicio [id=" + id + ", idTecnico=" + idTecnico + ", idServicio=" + idServicio + ", fechaInicio="
				+ fechaInicio + ", fechaFin=" + fechaFin + "]";
	}	
}
